#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define _GNU__SOURCE

typedef struct present
{
	size_t l, w, h;
} present_t;

present_t parse_present(char *str);

size_t compute_wrapping(present_t present);

size_t compute_ribbon(present_t present);

int main(void)
{
	size_t total_wrapping = 0;
	size_t total_ribbon = 0;
	
	char *line = NULL;
	size_t size = 0;
	while ((size = getline(&line, &size, stdin)) != -1)
	{
		line[size - 1] = '\0';

		present_t present = parse_present(line);
		total_wrapping += compute_wrapping(present);
		total_ribbon += compute_ribbon(present);
	}
	free(line);

	if (!feof(stdin))
	{
		fputs(strerror(errno), stderr);
		return EXIT_FAILURE;
	}

	printf("total wrapping: %zu\n", total_wrapping);
	printf("total ribbon: %zu\n", total_ribbon);

	return EXIT_SUCCESS;
}

present_t parse_present(char *str)
{
	// FIXME: No error-handling!
	size_t l = strtoul(strtok(str, "x"), NULL, 10);
	size_t w = strtoul(strtok(NULL, "x"), NULL, 10);
	size_t h = strtoul(strtok(NULL, "\0"), NULL, 10);

	return (present_t) {
		.l = l,
		.w = w,
		.h = h,
	};
}

size_t min(size_t a, size_t b)
{
	return a < b ? a : b;
}

size_t compute_wrapping(present_t present)
{
	size_t side1 = present.l*present.w;
	size_t side2 = present.w*present.h;
	size_t side3 = present.h*present.l;

	return 2*side1 + 2*side2 + 2*side3 + min(min(side1, side2), side3);
}

size_t compute_ribbon(present_t present)
{
	size_t perimiter1 = 2*present.l + 2*present.w;
	size_t perimiter2 = 2*present.w + 2*present.h;
	size_t perimiter3 = 2*present.h + 2*present.l;
	size_t volume = present.l*present.w*present.h;
	
	return volume + min(min(perimiter1, perimiter2), perimiter3);
}

