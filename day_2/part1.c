#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#define _GNU__SOURCE

typedef struct present
{
	size_t l, w, h;
} present_t;

present_t parse_present(char *str);

size_t compute_wrapping(present_t present);

int main(void)
{
	size_t total_wrapping = 0;
	
	char *line = NULL;
	size_t size = 0;
	while ((size = getline(&line, &size, stdin)) != -1)
	{
		line[size - 1] = '\0';

		total_wrapping += compute_wrapping(parse_present(line));
	}
	free(line);

	if (!feof(stdin))
	{
		fputs(strerror(errno), stderr);
		return EXIT_FAILURE;
	}

	printf("total wrapping: %zu\n", total_wrapping);

	return EXIT_SUCCESS;
}

present_t parse_present(char *str)
{
	// FIXME: No error-handling!
	size_t l = strtoul(strtok(str, "x"), NULL, 10);
	size_t w = strtoul(strtok(NULL, "x"), NULL, 10);
	size_t h = strtoul(strtok(NULL, "\0"), NULL, 10);

	return (present_t) {
		.l = l,
		.w = w,
		.h = h,
	};
}

size_t min(size_t a, size_t b)
{
	return a < b ? a : b;
}

size_t compute_wrapping(present_t present)
{
	size_t side1 = present.l*present.w;
	size_t side2 = present.w*present.h;
	size_t side3 = present.h*present.l;

	return 2*side1 + 2*side2 + 2*side3 + min(min(side1, side2), side3);
}

