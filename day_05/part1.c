#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdbool.h>
#include <assert.h>

#define _GNU_SOURCE

bool is_nice(const char *str);

int main(void)
{
	assert(is_nice("ugknbfddgicrmopn"));
	assert(is_nice("aaa"));
	assert(!is_nice("jchzalrnumimnmhp"));
	assert(!is_nice("haegwjzuvuyypxyu"));
	assert(!is_nice("dvszwmarrgswjxmb"));

	char *line = NULL;
	size_t size = 0;

	unsigned int nice_counter = 0;
	while (getline(&line, &size, stdin) != -1)
	{
		line[strlen(line) - 1] = '\0'; // Remove nl

		if (is_nice(line))
		{
			++nice_counter;
		}
	}

	if (!feof(stdin))
	{
		fputs(strerror(errno), stderr);
	}

	// 258
	printf("nice strings: %u\n", nice_counter);
	return EXIT_SUCCESS;
}

bool is_vowel(char c)
{
	return index("aeiou", c) != NULL;
}

bool starts_with_double(const char *str)
{
	if (*str == '\0')
	{
		return false;
	}

	return *str == *(str + 1);
}

bool starts_with_one_of(const char **patterns, size_t size, const char *str)
{
	for (size_t i = 0; i < size; ++i)
	{
		const char *p = patterns[i];
		if (strncmp(p, str, strlen(p)) == 0)
		{
			return true;
		}		
	}

	return false;
}

bool is_nice(const char *str)
{
	size_t vowel_count = 0;
	bool has_double = false;
	bool has_bad_string = false;

	static const char *bad_strings[] = {
		"ab",
		"cd",
		"pq",
		"xy",
	};

	char c;
	while ((c = *str) != '\0')
	{
		if (is_vowel(c))
		{
			++vowel_count;
		}

		if (starts_with_double(str))
		{
			has_double = true;
		}

		if (starts_with_one_of(bad_strings, sizeof(bad_strings)/sizeof(bad_strings[0]), str))
		{
			has_bad_string = true;
		}

		++str;
	}

	return vowel_count >= 3 && has_double && !has_bad_string;
}

