#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct
{
	int x, y;
} position_t;

/* Dictionary */
#define DICT_SIZE 256

typedef position_t element_t;

void init_dict();

bool lookup(element_t elem);

void insert(element_t elem);

int main(void)
{
	init_dict();

	position_t pos1 = (position_t) { 0, 0 };
	position_t pos2 = (position_t) { 0, 0 };
	insert(pos1);

	size_t visited = 1;
	char c;
	while ((c = getchar()) != EOF)
	{
		switch (c)
		{
		case '^':
			++pos1.y;
			break;
		case 'v':
			--pos1.y;
			break;
		case '>':
			++pos1.x;
			break;
		case '<':
			--pos1.x;
			break;
		}

		if (!lookup(pos1))
		{
			insert(pos1);
			++visited;
		}

		position_t tmp = pos1;
		pos1 = pos2;
		pos2 = tmp;
	}
	
	printf("houses visited: %zu\n", visited);
	return EXIT_SUCCESS;
}

typedef struct bucket
{
	element_t elem;
	struct bucket *next;
} bucket_t;

bucket_t *dictionary[DICT_SIZE];

void init_dict()
{
	memset(dictionary, 0, DICT_SIZE*sizeof(bucket_t*));
}

size_t hash(element_t elem)
{
	return (size_t)(elem.x + elem.y) % DICT_SIZE;
}

bool lookup(element_t elem)
{
	bucket_t *buck = dictionary[hash(elem)];

	while (buck != NULL)
	{
		if (buck->elem.x == elem.x && buck->elem.y == elem.y)
		{
			return true;
		}
		buck = buck->next;
	}

	return false;
}

void insert(element_t elem)
{
	size_t i = hash(elem);

	bucket_t *buck = dictionary[i];
	bucket_t *new_buck = malloc(sizeof(bucket_t));
	if (new_buck == NULL)
	{
		exit(EXIT_FAILURE);
	}

	new_buck->elem = elem;	
	new_buck->next = buck;
	dictionary[i] = new_buck;
}


