#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int level = 0;
	char c;
	while ((c = getchar()) != EOF)
	{
		switch (c)
		{
		case '(':
			++level;
			break;
		case ')':
			--level;
			break;
		}
	}

	printf("level: %d\n", level);
	return EXIT_SUCCESS;
}

