#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int level = 0,
		position = 0,
		basement_pos = 0;
	char c;

	while ((c = getchar()) != EOF)
	{
		++position;

		switch (c)
		{
		case '(':
			++level;
			break;
		case ')':
			--level;
			break;
		}

		if (level == -1 && basement_pos == 0)
		{
			basement_pos = position;
		}
	}

	printf("basement: %d\n", basement_pos);
	printf("level: %d\n", level);
	return EXIT_SUCCESS;
}

